﻿
window.addEventListener("load", loadHandler, false);

var getFringeInfo = getFringeInfo_3;
var getFringes = getFringes_1;


function loadHandler(){

	storage = {};
	make_storage(storage);
	
	var form = document.forms["input_form"];
	
	var button_add = form["add_button"];
	var button_del_all = form["del_all_button"];	

	button_add.addEventListener("click", addNoteHandler, false);
	button_del_all.addEventListener("click", delAllNotesHandler, false);
	
	var arr = document.getElementsByClassName("note");
	
	for (var i = 0; i < arr.length; i++) {
		
		arr[i].addEventListener("mouseover", note_mouseover, false);
		arr[i].addEventListener("mouseout", note_mouseout, false);
		arr[i].addEventListener("click", note_click, false);
		
	}
	
	var dynamicForm = new FormData();
	
	dynamicForm.append("action","getNotes");
	//dynamicForm.append("limit", 'all');
	
	postForm(dynamicForm, xhrGetNotesHandler);
	
	var notes = document.querySelector('.notes');
	var button_show_all = document.querySelector('#showAll');
	button_show_all.addEventListener('click',showAll);
	
	var options = {
		root: null
	}
	
	var observer = new IntersectionObserver(callbackObserver, options);
	storage.set('observer', observer);
	
}

function showAll(event) {
	
	var output  = document.querySelector(".notes");

	var observer = storage.get('observer');
	observer.unobserve(output.lastElementChild);
	storage.set('observer', observer);	
	
		
	var dynamicForm = new FormData();
	
	dynamicForm.append("action","getNotes");
	dynamicForm.append("limit", 'all');
	
	postForm(dynamicForm, xhrGetNotesHandler);
	
}

function callbackObserver(entries, observer) { 

	if (entries[0].isIntersecting == true) {
		
		var output = document.querySelector('.notes');
		/*
		console.log('callback');
		console.log('entries',entries);
		console.log('observer',observer);
		console.log('this',this);
		*/
		observer.unobserve(output.lastElementChild);

		var dynamicForm = new FormData();
	
		dynamicForm.append("action","getNotes");
		var id_note = storage.get('id_note');
		dynamicForm.append("id_note", id_note);
		
		postForm(dynamicForm, xhrGetNotesHandler);

	}
	
};

function xhrGetNotesHandler(event){
		var output  = document.querySelector(".notes");
		var loader = output.querySelector("#loader");
		
		if (loader) {
			output.removeChild(loader);
		}
		
		if (this.responseText!="") {

			var obj = JSON.parse(this.responseText);

			if (obj['limit']=='all') {
				
				var button = document.querySelector('#showAll');
				var parent = button.parentElement;
				parent.removeChild(button);
				
			}
			
			for (var i =0; i <obj['notes'].length; i++) {
				
				obj['notes'][i]["title"] = escapeHTML(obj['notes'][i]["title"]);

				var str='\
					<div id="'+obj['notes'][i]["id_note"] +'" class="note">\
						<div class="title_note">\
							<h2 class="title">'+obj['notes'][i]["title"]+'</h2>\
							<div class="menu">\
								<img src="icons/edit_note_0.png" class="edit_note"><img src="icons/delete_note_0.png" class="delete_note">\
							</div>\
						</div>\
						<div class="content_note\"><div class="body_note">'
								+obj['notes'][i]["content"]+
						'</div></div>\
					</div>';
					
				var el = document.createElement("div");
				el.innerHTML= str;
				el = el.firstElementChild;
				
				el.addEventListener("mouseover", note_mouseover, false);
				el.addEventListener("mouseout", note_mouseout, false);
				el.addEventListener("click", note_click, false);
				
				output.appendChild(el);		
				
			}
			
			if (Object.keys(obj['notes']).length>0) {
				
				var id_note = obj['notes'][obj['notes'].length-1]["id_note"];
				storage.set('id_note',id_note);
				var observer = storage.get('observer');
				storage.set('observer', observer);
				observer.observe(output.lastElementChild);
				
			}

		}
	
}

function note_mouseover(event) {
	
	var arr = {
		"delete_note" : "icons/delete_note_1.png",
		"edit_note" : "icons/edit_note_1.png",
		"cancel_note" : "icons/cancel_note_1.png",
		"save_note" : "icons/save_note_1.png"
	};
	
	var key = event.target.className;
	
	if (key in arr) {
		event.target.src = arr[key];
	}
	
}

function note_mouseout(event) {
	
	var arr = {
		"delete_note" : "icons/delete_note_0.png",
		"edit_note" : "icons/edit_note_0.png",
		"cancel_note" : "icons/cancel_note_0.png",
		"save_note" : "icons/save_note_0.png"
	};
	
	var key = event.target.className;
	
	if (key in arr) {
		event.target.src = arr[key];
	}

}

function make_storage(obj) {
	
	var value = {};
	
	obj["get"] = function(key) {
		var tmp = value[key];
		delete  value[key];
		return tmp;
	};
	
	obj["set"] = function(key,v) { 
		value[key] = v;
	};
	
}


function delete_note(event){
	
	event.stopPropagation();
	event.preventDefault();
	
	var dynamicForm = new FormData();
	
	dynamicForm.append("action", "deleteNote");
	dynamicForm.append("id", event.currentTarget.id);
	
	postForm(dynamicForm, xhrDelNoteHandler);
	
}

function edit_note(event){ 
	
	var title_note = event.currentTarget.children[0].firstElementChild;
	var content_note = event.currentTarget.children[1].firstElementChild;
	
	var obj= {};
	var s = [];
	

	obj["title_note"] = nodeToObject(title_note);
	obj["content_note"] = nodeToObject(content_note);

	storage.set(event.currentTarget.id, obj);
	
	title_note.setAttribute("contenteditable","true");
	content_note.setAttribute("contenteditable","true");

	event.target.parentElement.innerHTML =
	"<img src=\"icons/save_note_0.png\" class=\"save_note\">"+
	"<img src=\"icons/cancel_note_0.png\" class=\"cancel_note\">";
	
	body_note_click_add(event);
	title_click_add(event);
	
	var str ="";
	
}



function pasteBuffer (e){
	/*
	"text/plain"
	"text/html"
	*/
	var text;
	if (e.clipboardData){
		
		text = e.clipboardData.getData("text/plain");

		//alert(text);
		insert(e, text);
		e.preventDefault();
       // We are already handling the data from the clipboard, we do not want it inserted into the document
    } else if (window.clipboardData) {
		
		text = window.clipboardData.getData("Text");

		//alert(text);
		insert(e, text);
		var select = window.getSelection();
		var range = select.getRangeAt(0);
		e.stopPropagation();
		e.preventDefault();
	} 
}
function processDataFromClipboard(data) {
	alert(data);
}

function cancel_note(event){ 
	var obj = storage.get(event.currentTarget.id);
	
	var title_note = event.currentTarget.children[0].firstElementChild;
	var content_note = event.currentTarget.children[1].firstElementChild;
	
	title_note.innerHTML = objectToHtml(obj["title_note"]);
	content_note.innerHTML = objectToHtml(obj["content_note"]);
	
	
	title_note.setAttribute("contenteditable","false");
	content_note.setAttribute("contenteditable","false");
	
	event.target.parentElement.innerHTML =
	"<img src=\"icons/edit_note_0.png\" class=\"edit_note\">"+
	"<img src=\"icons/delete_note_0.png\" class=\"delete_note\">";
	
	title_click_remove(event);
	body_note_click_remove(event);
	
}

function save_note(event){ 

	var title_note = event.currentTarget.children[0].firstElementChild;
	var content_note = event.currentTarget.children[1].firstElementChild;
	title_note = title_note.textContent;

	content_note = nodeToObject(content_note);
	content_note = JSON.stringify(content_note);

	var dynamicForm = new FormData();
	
	dynamicForm.append("action","updateNote");
	dynamicForm.append("id", event.currentTarget.id);
	dynamicForm.append("title", title_note);
	dynamicForm.append("note", content_note);
	
	postForm(dynamicForm, xhrUpdateNoteHandler);
	title_click_remove(event);
	body_note_click_remove(event);
}



function title_click_add(event){ 
	var element = event.currentTarget.children[0].firstElementChild;
	element.addEventListener("keydown", titleKeyDown, true);
	element.addEventListener("keypress",titleKeyPress, true);
	element.addEventListener("keyup", titleKeyUp, true);
	element.addEventListener("mouseup", titleMouseUp, true);
}

function title_click_remove(event){ 
	var element = event.currentTarget.children[0].firstElementChild;
	element.removeEventListener("keydown", titleKeyDown, true);
	element.removeEventListener("keypress",titleKeyPress, true);
	element.removeEventListener("keyup", titleKeyUp, true);
	element.removeEventListener("mouseup", titleMouseUp, true);
}

function body_note_click_add(event){ 
	var element = event.currentTarget.children[1].firstElementChild;
	element.addEventListener("keydown", noteKeyDown, true);
	element.addEventListener("keypress",noteKeyPress, true);
	element.addEventListener("keyup", noteKeyUp, true);
	element.addEventListener("mouseup", noteMouseUp, true);
	element.addEventListener("paste", pasteBuffer);
}

function body_note_click_remove(event){ 
	var element = event.currentTarget.children[1].firstElementChild;
	element.removeEventListener("keydown", noteKeyDown, true);
	element.removeEventListener("keypress",noteKeyPress, true);
	element.removeEventListener("keyup", noteKeyUp, true);

	element.removeEventListener("mouseup", noteMouseUp, true);
	element.removeEventListener("paste", pasteBuffer);

}




function noteMouseUp(event){ 


}


function titleMouseUp(event){ 


}


//для обработки функциональных клавиш
function titleKeyDown(event){ 
/*
а - русская и английская f
KeyboardEvent

charCode: 0
keyCode: 70
keyIdentifier: "U+0046"
which: 70

type: (...)
view: (...)
which:


keyCode: 16
keyIdentifier: "Shift"
keyLocation: 1
location: 1

keyCode: 18
keyIdentifier: "Alt"
keyLocation: 1
location: 1



*/
	var key = event.key || event.keyIdentifier;
	var action = {
		"Enter": titleEnterDown
	};
	if (key in action) {
		action[key](event);
	}

}

function titleEnterDown(event){ 
	event.stopPropagation();
	event.preventDefault();
}

//для обработки печатных символов
function titleKeyPress(event){ 
/*
а - русская
KeyboardEvent
charCode: 1072
keyCode: 1072
keyIdentifier: "U+0430"
which: 1072

charCode: 102
keyCode: 102
keyIdentifier: "U+0046"
which: 102
*/
}

function titleKeyUp(event){ 

}





function note_click(event) {
	
	var element = event.target.className;
	
	var action = {
		"delete_note": delete_note,
		"edit_note": edit_note,
		"cancel_note": cancel_note,
		"save_note": save_note,

	};
	
	if (element in action) {
		action[element](event);
	}
	
	
}


//для обработки функциональных клавиш



function noteKeyDown(event){ 

	//event.stopPropagation();
	//event.preventDefault();
	
	var keyCodeObj = {
		13: "\n", // "Enter"
		9: "\t", //"Tab"
		46: "delete", //delete
		8: "backspace"
	};
	var keyFuncObj = {
		90: "z", // z
		88: "x", // x
		67: "c", // c
		86: "v", // v
	};
	
	var key = keyCodeObj[event.keyCode] ;
	if (key) {
		noteSpecialKeyDown(event, key);
	}
	
	if (event.altKey || event.shiftKey || event.ctrlKey || event.metaKey) {
		var funcKey = keyFuncObj[event.which];
		switch(funcKey) {
			case "c": {
				//alert("ctrl + c");
			} 
			break;
			case "v": {
				//alert("ctrl + v");
				/*
				var text = getClipboard();
				alert(text);
				var commande = "Paste";
				var valid = document.execCommand(commande , null, null);
				alert(valid);
				*/
				
				
				//event.stopPropagation();
				//event.preventDefault();
				
			} 
			break;
			case "z": {
				//alert("ctrl + z");
								
				event.stopPropagation();
				event.preventDefault();
			}
			break;
			case "x": {
				//alert("ctrl + z");
			}
			break;
			
		}
		
	}

	

}
function noteSpecialKeyDown(event, key){ 
 
	insert(event, key);

}


//для обработки печатных символов
function noteKeyPress(event){ 

if (!event.altKey && !event.shiftKey && !event.ctrlKey && !event.metaKey) {
	var key = "printable";
	noteSpecialKeyDown(event, key);
}
	
}

function noteKeyUp(event){ 

}



function getParents(elem, root) {
	
	var parents = [];

	while (elem !== root) {
		parents.push(elem);
		elem = elem.parentNode;
	}
	
	parents.push(root);
	
	return parents;
}




function typeElem(elem) {
	var str;
	if (
		(elem.nodeType == Node.ELEMENT_NODE) &&
		(elem.tagName.toLowerCase() == "a") &&
		(elem.hasAttribute("href") == true) &&
		(elem.childNodes.length == 1) &&
		(elem.childNodes[0].nodeType == Node.TEXT_NODE)
	) {
		str = "simple link";
	}
	if (
		elem.nodeType == Node.TEXT_NODE
	) {
		str = "simple text";
	}
	
	return str;
	
}

function typeSelected(parents) {
	var level = parents.length;
	var str;
	if (
			(level == 3) &&
			(parents[1].nodeType == Node.ELEMENT_NODE) &&
			(parents[1].tagName.toLowerCase() == "a") && 
			(parents[1].hasAttribute("href") == true) &&
			(parents[1].childNodes.length == 1) && 
			(parents[0].nodeType == Node.TEXT_NODE)
		)
	{
		str = "simple link";
	}
	if (
		(level == 2) &&
		(parents[0].nodeType == Node.TEXT_NODE)
	){
		str = "simple text";
	}
	return str;
}




function getFringes_1(parents, offset, direction){
	var iterator =
	{
		"left": "previousSibling",
		"right": "nextSibling"
	};
	
	fringe = -1;
	fringes = [];
	not_fringes = [];
	
	var next = iterator[direction];
	
	var state = true;
	var i = 0;
	var lastIndex = parents.length - 1; 
	var lastElemIndex = lastIndex - 1;
	while(state){
		if (i == 0) {
			if (parents[i].nodeType == Node.TEXT_NODE) {
				var rightOffset =  direction == "left" ? 0 : parents[i].length;
				if(offset == rightOffset ) { //проверить нет ли соседей например картинка  
					if(parents.length > 2) {
						if(parents[i][next] == null) {
							fringes.push(i);
							fringe = i;
						} else {
							not_fringes.push(i);
						}
					} else {
						fringes.push(i);
						fringe = i;
					}
				} else {
					not_fringes.push(i);
				}
			} else {
				fringes.push(i);
				fringe = i;
			}
		} else {
			if(parents[i][next] == null) { //если сосед равен нулю то это fringe
				fringes.push(i);
				if(not_fringes.length == 0) {
					fringe = i;
				}
			} else {
				not_fringes.push(i);
			}
		}
		i++;
		if (i > lastElemIndex) {
			state = false;
		}
	}	
	
	var obj  = {
		"fringe": fringe,
		"fringes": fringes,
		"not_fringes": not_fringes
	};
	
	return obj;
}

function getFringeInfo_3(fringe, length) {
	var typeFringe = {
		0: "notFringe",
		1: "partFringe",
		2: "fullFringe",
		3: "superFringe"
	};
	
	
	var type;
	var fringeElem;
	var countElem; //количество элементов 
	
	countElem = length;
	
	if (fringe == -1) {
		fringeElem = 0; 
	} else {
		if (length == 2) {
			fringeElem = fringe + 1;
		} else if (length > 2) {
			fringeElem = fringe + 2;
		}
	}
	
	if (fringeElem == 0) {
		type = 0; //notFringe
	} else if ( countElem - fringeElem > 1) {
		type = 1; //partFringe
	} else if ( countElem - fringeElem == 1){
		type = 2; //fullFringe
	} else if ( countElem - fringeElem == 0){
		type = 3; //superFringe
	}
				

	var obj =	{
		"type": type,
		"typeString": typeFringe[type], 
		"fringeElem": fringeElem,
		"countElem": countElem 
	};
	
	return obj;
	
}


function fringeChild(elem, direction){
	
	var fringeChild =[];
	var next;
	fringeChild.push(elem);
	
	while( elem.childNodes.length > 0 ) {
		next = (direction == "left") ? 0: elem.childNodes[elem.childNodes.length-1];
		elem = elem.childNodes[next];
		fringeChild.push(elem);
	}
	return fringeChild;
}






function getOffsetElem(elem, root){
	var i = 0;
	while (root.childNodes[i] != elem) {
		i++;
	}
	return i;
}

function getChar(event) {
  if (event.which == null) { // IE
    if (event.keyCode < 32) return null; // спец. символ
    return String.fromCharCode(event.keyCode)
  }

  if (event.which != 0 && event.charCode != 0) { // все кроме IE
    if (event.which < 32) return null; // спец. символ
    return String.fromCharCode(event.which); // остальные
  }

  return null; // спец. символ
}

function insert(event, key) {
	var select = window.getSelection();
	var range = select.getRangeAt(0);
	var collapsed = range.collapsed;
	

	//определяем тип вставляемого контетна
	var type_insert;
	if (typeof(key) =="string") {
		type_insert="simple text";
		var type = {
			"\n":"insert",
			"\t":"insert",
			"printable": "insert",
			"delete":"remove",
			"backspace": "remove"
		}
		var type_key; 
		if(type[key]) {
			type_key = type[key];
		} else if (typeof(key) =="string" && key.length > 0){
			type_key = "insert";
		}
		
		if(key =="printable") {
			type_key = "insert";
			key = getChar(event);
		}
	} else if (typeof(key) === "undefined"){
		type_insert="undefined key";
	}

	
	
	var reverse = false;
	if (collapsed == false) {
		reverse = (range.startContainer == select.focusNode) ? true: false;
	}
	
	
	var leftElem = range.startContainer;
	var rightElem = range.endContainer;
	var startOffset =  range.startOffset; //косяк браузера
	var endOffset =  range.endOffset;
	
			
	var typeSelect;
	var selectedElem;
	
	var root = event.currentTarget;

	
	var leftFringe;
	var rightFringe;
	
	var str_new="";
	var str="";
	
	var obj;
	var need_change = true;
	var default_change = false;
	var select_range;
	if(collapsed == false) {
		select_range = true;
	} else {
		select_range = false;
	}
	
	if (type_insert =="undefined key") {
		
	} else {
		
		if (collapsed == false) {
			//режим выделения
			if(leftElem == rightElem) { 
				var mainElem = leftElem;
				var mainParents = getParents(mainElem, root);
				
				//problems with into links
				if (type_key == "remove") {
					//need_change = false;
				}
				
				if (mainElem.nodeType == Node.TEXT_NODE) {
					
					if (mainParents.length > 2) {
						var new_obj = {};
						if(range.startOffset == 0) {	
							leftFringe = getFringes(mainParents, range.startOffset, "left"); 
							new_obj[0] = getFringeInfo(leftFringe["fringe"], mainParents.length);
						}
						if (range.endOffset == mainElem.length) {
							rightFringe = getFringes(mainParents, range.endOffset, "right");
							new_obj[1] = getFringeInfo(rightFringe["fringe"], mainParents.length);
						}

						if(
							(new_obj[0] && new_obj[0]["type"] >= 2) &&
							(new_obj[1] && new_obj[1]["type"] >= 2) 
						){ 
							var mainElemRoot = mainParents[mainParents.length -2 ];
							range.selectNode(mainElemRoot);
							range.deleteContents();
							var offset = 0;
							if(range.startOffset == range.startContainer.childNodes.length) {
								offset = range.startContainer.childNodes.length - 1;
								range.setStart(range.startContainer, offset);
								range.setEnd(range.startContainer, offset);
								
							}
							
							if(
								(range.startContainer == root)&&
								(range.endContainer == root)&&
								(range.startOffset == range.endOffset) &&
								(range.startContainer.childNodes[range.startOffset].nodeType == Node.TEXT_NODE)
							){
								mainElemRoot = range.startContainer.childNodes[range.startOffset];
								if(offset > 0) {
									offset = mainElemRoot.length;
								}
								range.setStart(mainElemRoot, offset);
								range.setEnd(mainElemRoot, offset);
								
							} else {
								startOffset =  range.startOffset;
								endOffset =  range.endOffset;
							}
								

							select.removeAllRanges();
							select.addRange(range);
							if(
								(type_key == "remove") 
							){
								need_change = false;
								default_change = false;
							}
							//need_change = false;

						} else if(new_obj[0] && new_obj[0]["type"] >= 2){ 
							//слева есть граница
							rightElem = mainParents[mainParents.length -2 ];
							leftElem = rightElem.previousSibling;
							if(new_obj[0]["type"] ==3) {
								
								range.deleteContents();
								
							} else {
								range.setStartAfter(leftElem);
								range.deleteContents();
								//new

								
							}
								leftElem = range.startContainer;
								rightElem = range.endContainer;
								startOffset =  range.startOffset - 1; //косяк браузера
								endOffset =  range.endOffset;
							
		
							
						} else if(new_obj[1]  && new_obj[1]["type"] >= 2) {
							//справа есть граница
							
							
							leftElem = mainParents[mainParents.length -2 ];
							rightElem = leftElem.nextSibling;
							
							if(new_obj[1]["type"] ==3) {
								
								range.deleteContents();
								
							} else {
								range.setEndBefore(rightElem);
								range.deleteContents();
							}	
							
							//new
							leftElem = range.startContainer;
							rightElem = range.endContainer;
							startOffset =  range.startOffset - 1; //косяк браузера
							endOffset =  range.endOffset;
						} else {
							
							if(
								(key == "\n") ||(key == "\t")
							){
								range.deleteContents();
								need_change = false;
								default_change = false;
							}else if (event.type=="paste"){
								range.deleteContents();
								need_change = true;
								default_change = true;
							} else {
								change_need = false;
								default_change = true;
							}
							
							
						}
						
					} else {
						
						//Если выделен весь текст то
						if (
							(range.startOffset == 0)&&
							(range.endOffset == mainElem.length)
						) {
							
							range.selectNode(mainElem);
							range.deleteContents();
							startOffset =  range.startOffset - 1; //косяк браузера
							endOffset =  range.endOffset;
						}else {
							
							if(
								(key == "backspace") ||(key == "delete")
							){
								range.deleteContents();
								need_change = false;
								default_change = false;
							}else {
								range.deleteContents();
								change_need = true;
								default_change = false;
							}
						}
						
					}
					//удалить 
				} else {
					//картинка
				}
				
			}
			if (leftElem != rightElem) {
				range.deleteContents();
				if (range.startOffset > 0) {
					startOffset =  range.startOffset - 1; //косяк браузера
				} else {
					startOffset =  range.startOffset; 
				}
				
				endOffset =  range.endOffset;	
				if(
					(range.startContainer == root) &&
					(range.endContainer == root) 
				){
					leftElem = root.childNodes[startOffset];
					rightElem = root.childNodes[endOffset];
					
					if(typeElem(leftElem) =="simple link") {
						range.setStart(leftElem.childNodes[0], leftElem.childNodes[0].length);
						range.setEnd(leftElem.childNodes[0], leftElem.childNodes[0].length);
					} else if(typeElem(leftElem) =="simple text") {  
						range.setStart(leftElem, leftElem.length);
						range.setEnd(leftElem, leftElem.length);
					}
					
				}
			} 
		}
		
		collapsed = true;
		reverse = false;
		leftElem = range.startContainer;
		rightElem = range.endContainer;
		
		if (collapsed == true && need_change == true) { // режим кусрсора
			if(leftElem == rightElem) { //если текст то возможно нормально все
					//узнаем левые и правые границы
					
				//если текст мы можем быть внутри сложного объекта
				var mainElem = leftElem;
				var mainParents = getParents(mainElem, root);
				if (mainElem.nodeType == Node.TEXT_NODE) {
		
					var new_obj = {};
					if(range.startOffset == 0) {	
						leftFringe = getFringes(mainParents, range.startOffset, "left"); 
						new_obj[0] = getFringeInfo(leftFringe["fringe"], mainParents.length);
					}
					if (range.endOffset == mainElem.length) {
						rightFringe = getFringes(mainParents, range.endOffset, "right");
						new_obj[1] = getFringeInfo(rightFringe["fringe"], mainParents.length);
					}
					
					
					var offset_elem;
					
					if(
						(new_obj[0] && new_obj[0]["type"] >=2)&&
						(new_obj[1] && new_obj[1]["type"] >=2)
					){ 
						var mainElem = mainParents[mainParents.length - 2];

					
					}else if(new_obj[0] && new_obj[0]["type"] >=2){ //левый элемент

					if (mainParents[mainParents.length - 2].previousSibling != null) {
							rightElem = mainParents[mainParents.length - 2];
							leftElem = rightElem.previousSibling;	
							
							if(type_key == "insert" ){ 
								if (leftElem != null) {
									startOffset = getOffsetElem(leftElem, root);
									endOffset = getOffsetElem(rightElem, root);
									range.setStart(root, startOffset);
									range.setEnd(root, endOffset);
									mainElem = root;
								}
							} else if (type_key == "remove" ){ 
								if(key =="delete") {
									if( typeElem(rightElem) == "simple link") {
										if (rightElem.childNodes[0].length >1 ) {
											range.setStart(rightElem.childNodes[0], 0);
											range.setEnd(rightElem.childNodes[0], 0);
											default_change = true;
										} else {
											range.selectNode(rightElem);
											range.deleteContents();
											select.removeAllRanges();
											select.addRange(range);
											if(range.startContainer == root) {
												mainElem = range.startContainer;
												startOffset = range.startOffset - 1;
												endOffset = range.endOffset;
											}
											 
											//склейка
										}
									} else if (typeElem(rightElem) == "simple text"){
										default_change = true; 
									}
								} else if(key =="backspace") {
									if( typeElem(leftElem) == "simple text") {
										default_change = true; 
									} else if( typeElem(leftElem) == "simple link") {
										if (leftElem.childNodes[0].length >1 ) {
											range.setStart(leftElem.childNodes[0], leftElem.childNodes[0].length);
											range.setEnd(leftElem.childNodes[0], leftElem.childNodes[0].length);
											default_change = true;
										} else {
											range.selectNode(leftElem);
											range.deleteContents();
											select.removeAllRanges();
											select.addRange(range);
											if(range.startContainer == root) {
												mainElem = range.startContainer;
												startOffset = range.startOffset - 1;
												endOffset = range.endOffset;
											}
										}
									}
								} 
							}
							
						} else {
							//определяем тип объекта справа
							rightElem = mainParents[mainParents.length - 2];
							var type_right = typeSelected(mainParents);
							if(type_key == "insert" ){ //key!=backspace
								
								if (type_right == type_insert){
									rightElem.textContent = key + rightElem.textContent;
									offset_elem =  key.length;
									range.setEnd(rightElem,  key.length);
									range.setStart(rightElem,  key.length);
									select.removeAllRanges();
									select.addRange(range);
								} else {
									//закрыть баг
									var newElem = document.createTextNode(key);
									if(type_right == "simple link"){
										
										var parentElem = rightElem.parentNode;
										parentElem.insertBefore(newElem, rightElem);
										range.setStart(newElem,  key.length);
										range.setEnd(newElem,  key.length);
										select.removeAllRanges();
										select.addRange(range);
									}
								}
							} else if(type_key == "remove"){ 
								if(key =="delete") { 
										if( type_right == "simple link") {
											if (rightElem.childNodes[0].length >1 ) {
												range.setStart(rightElem.childNodes[0], 0);
												range.setEnd(rightElem.childNodes[0], 0);
												default_change = true;
											} else {
												range.selectNode(rightElem); 
												range.deleteContents();
												select.removeAllRanges();
												select.addRange(range);
											}
										} else {
											default_change = true;
										}
								} else if (key =="backspace") {
									if( typeElem(leftElem) == "simple text") { //?
										range.setStart(leftElem, leftElem.textContent.length);
										range.setEnd(leftElem, leftElem.textContent.length);
										default_change = true; 
									}
								}
							}
							//игнорируем backspace
						}
					
					} else if (new_obj[1]  && new_obj[1]["type"] >=2) { //правый элемент
					

						if (mainParents[mainParents.length - 2].nextSibling != null) {
							leftElem = mainParents[mainParents.length - 2];
							rightElem = leftElem.nextSibling;
							
							if(type_key == "insert" ){ 
								if (rightElem != null) {
									startOffset = getOffsetElem(leftElem, root);
									endOffset = getOffsetElem(rightElem, root);
									range.setStart(root, startOffset);
									range.setEnd(root, endOffset);
									mainElem = root;
								}
							} else if (type_key == "remove" ){ 
								if(key =="backspace") {
									if( typeElem(leftElem) == "simple link") {
										if (leftElem.childNodes[0].length >1 ) {
											range.setStart(leftElem.childNodes[0], leftElem.childNodes[0].length);
											range.setEnd(leftElem.childNodes[0], leftElem.childNodes[0].length);
											default_change = true;
										} else {
											range.selectNode(leftElem);
											range.deleteContents();
											select.removeAllRanges();
											select.addRange(range);
											//склеить
										}
									} else if( typeElem(leftElem) == "simple text") { 
										range.setStart(leftElem, leftElem.length);
										range.setEnd(leftElem, leftElem.length);
										default_change = true;
									}
								} else if(key =="delete") {
									if( typeElem(rightElem) == "simple text") {
										range.setStart(rightElem, 0);
										range.setEnd(rightElem, 0);
										default_change = true;
									}else if( typeElem(rightElem) == "simple link") {

										if (rightElem.childNodes[0].length >1 ) {
											range.setStart(rightElem.childNodes[0], rightElem.childNodes[0].length);
											range.setEnd(rightElem.childNodes[0], rightElem.childNodes[0].length);
											default_change = true;
										} else {
											range.selectNode(rightElem);
											range.deleteContents();
											select.removeAllRanges();
											select.addRange(range);
											if(range.startContainer == root) {
												mainElem = range.startContainer;
												if (range.startOffset > 0) {
													startOffset = range.startOffset - 1; 
												}
												endOffset = range.endOffset;
											}
											//склеить
										}
									}
								} 
							}
							
						} else {
							//определяем тип объекта слева
							leftElem = mainParents[mainParents.length - 2];
							var type_left = typeSelected(mainParents);
							if(type_key == "insert"){  //key!=del
								
								if (type_left == type_insert){
									if (
										(key=="\n") &&
										(leftElem.textContent[leftElem.textContent.length-1] != "\n" ) 
									){
										key+=key;
									}
									if (key!="\n") {
										var str = leftElem.textContent;
										if (str.substring(str.length-2, str.length)=="\n\n"){
											leftElem.textContent = str.substring(0, str.length-1);
										}
										
									}
									leftElem.textContent = leftElem.textContent + key;
									range.setEnd(leftElem,  leftElem.textContent.length);
									range.collapse(false);
									select.removeAllRanges();
									select.addRange(range);
									select.collapseToEnd();
									
								} else {
									//закрыть баг

									if(type_left == "simple link"){
										var text = key+key;
										var newElem = document.createTextNode(text);
										
										var parentElem = leftElem.parentNode;
										parentElem.insertBefore(newElem, leftElem.nextSibling);
										range.setStart(newElem,  text.length);
										range.setEnd(newElem,  text.length);
										select.removeAllRanges();
										select.addRange(range);
									}
								}
							} else if(type_key == "remove"){
								if(key =="delete") {
										if( type_right == "simple link") {
											if (rightElem.childNodes[0].length >1 ) {
												range.setStart(rightElem.childNodes[0], 0);
												range.setEnd(rightElem.childNodes[0], 0);
												default_change = true;
											} else {
												range.selectNode(leftElem); //rightElem?
												range.deleteContents();
												select.removeAllRanges();
												select.addRange(range);
											}
										}
								} else if(key =="backspace") {
									if( typeElem(leftElem) == "simple text") {
										default_change = true; 
									} else if( typeElem(leftElem) == "simple link") {
										if (leftElem.childNodes[0].length >1 ) {
											range.setStart(leftElem.childNodes[0], leftElem.childNodes[0].length);
											range.setEnd(leftElem.childNodes[0], leftElem.childNodes[0].length);
											default_change = true;
										} else {
											range.selectNode(leftElem);
											range.deleteContents();
											select.removeAllRanges();
											select.addRange(range);
											if(range.startContainer == root) {
												mainElem = range.startContainer;
												startOffset = range.startOffset - 1;
												endOffset = range.endOffset;
											}
										}
									}
								} 
							}
						}
							//игнорируем del, delete
					
					} else {

						if(type_key =="insert"){
							var type_main = typeSelected(mainParents);
							///test for 1
								//if (type_main == type_insert){
								if (type_main == type_insert){
									var str = mainElem.textContent;
									var left_text = str.substring(0, range.startOffset );
									var right_text = str.substring(range.startOffset, str.length );
									mainElem.textContent = left_text + key + right_text;
									
									range.setStart(mainElem,  left_text.length+ key.length);
									range.setEnd(mainElem,  left_text.length+ key.length);
									range.collapse(true);
									select.removeAllRanges();
									select.addRange(range);
									//select.collapseToEnd();
								} else if (key!="\n" && key!="\t"){
									var str = mainElem.textContent;
									var left_text = str.substring(0, range.startOffset );
									var right_text = str.substring(range.startOffset, str.length );
									mainElem.textContent = left_text + key + right_text;
									
									range.setStart(mainElem,  left_text.length+ key.length);
									range.collapse(true);
									select.removeAllRanges();
									select.addRange(range);
									select.collapseToEnd();
								}
						} else if (type_key =="remove"){
							default_change = true;
						}
						
					}
				
				} if (mainElem.nodeType == Node.ELEMENT_NODE) { 
				
					if(
						(range.startOffset == range.endOffset) &&
						(range.startOffset == 0) &&
						(range.startContainer.childNodes.length == 0)
					) {
						mainContainer = range.startContainer;
						
						if(type_key =="insert"){ 
							if (key =="\n") {
								key= key + key;
							} 
							var newElem = document.createTextNode(key);
							mainContainer.appendChild(newElem);
							range.setStart(newElem,  key.length);
							range.setEnd(newElem,  key.length);
							select.removeAllRanges();
							select.addRange(range);
						}
						
					} else {
						leftElem = range.startContainer.childNodes[startOffset];
						rightElem = range.endContainer.childNodes[endOffset];
						mainContainer = range.startContainer;
						if (mainElem == root) {
							//if(type_insert == "simple text"){ 
							if(type_key =="insert"){
								//проверить левый и правый элементы
								var index = 0;
								var arr_str=[];
								arr_str[1] = key;
								if (leftElem.nodeType == Node.TEXT_NODE) {
									arr_str[0] = leftElem.textContent;
									
								}
								if (rightElem.nodeType == Node.TEXT_NODE) {
									arr_str[2] =	rightElem.textContent;
									
								}
								var str = arr_str.join("");

								if (arr_str[0]) {

									if (arr_str[2]) {
										mainContainer.removeChild(rightElem);
									}
									leftElem.textContent = str;
									range.setStart(leftElem,  arr_str[0].length+key.length);
									range.setEnd(leftElem,  arr_str[0].length+key.length);
								} else if (arr_str[2]) {
									rightElem.textContent = str;
									range.setStart(rightElem,  key.length);
									range.setEnd(rightElem,  key.length);
								} else {
									var newElem = document.createTextNode(key);
									mainContainer.insertBefore(newElem, rightElem);
									range.setStart(newElem, key.length);
									range.setEnd(newElem, key.length);
									//сделать range
								}
								select.removeAllRanges();
								select.addRange(range);
								
							//} else if (type_insert == "undefined key") {
							}else if(type_key =="remove"){
								if (
									(startOffset != endOffset) &&
									(leftElem.nodeType == Node.TEXT_NODE) &&
									(rightElem.nodeType == Node.TEXT_NODE)
								){
									var offset = leftElem.textContent.length;
									var str = rightElem.textContent;
									mainContainer.removeChild(rightElem);		
									leftElem.textContent = leftElem.textContent + str;
									range.setStart(leftElem,  offset);
									range.setEnd(leftElem,  offset);
									//select.removeAllRanges();
									//select.addRange(range);
								}else if 	(leftElem.nodeType == Node.TEXT_NODE) {
									range.setStart(leftElem,  leftElem.textContent.length);
									range.setEnd(leftElem,  leftElem.textContent.length);
								} else if (rightElem.nodeType == Node.TEXT_NODE) {
									range.setStart(rightElem,  0);
									range.setEnd(rightElem, 0);
								}if (
									(leftElem == rightElem) &&
									(key =="delete") 
								){ 
									if( typeElem(rightElem) == "simple text") {
										range.setStart(rightElem, 0);
										range.setEnd(rightElem, 0);
										default_change = true;
									}else if( typeElem(rightElem) == "simple link") {

										if (rightElem.childNodes[0].length >1 ) {
											range.setStart(rightElem.childNodes[0], 0);
											range.setEnd(rightElem.childNodes[0], 0);
											default_change = true;
										} else {
											range.selectNode(rightElem);
											range.deleteContents();
											select.removeAllRanges();
											select.addRange(range);
											if(range.startContainer == root) {
												mainElem = range.startContainer;
												if (range.startOffset > 0) {
													startOffset = range.startOffset - 1; 
												}
												endOffset = range.endOffset;
											}
											//склеить
										}
									}
								}
								select.removeAllRanges();
								select.addRange(range);

							}
							
						}
					}

				}
				
			} else if(leftElem != rightElem) {
				
			}
				
		} 
		
		
		if (event.type!="paste") {
			if(default_change == false){ //перехват в самом конце
				event.stopPropagation();
				event.preventDefault();
			}
		}	
	}

}


function toHTML(docFragment){
	
  var d = document.createElement('div');
  d.appendChild(docFragment);
  return d.innerHTML;
  
}


function objectToHtml(arr){
	
	var str="";
	//escapeHTML()
	for (var i = 0; i < arr.length; i++) {
		
		switch (arr[i]["type"]) {
			
			case "a": {
				str+="<a href=\"" + escapeHTML(arr[i]["href"]) + "\">" + escapeHTML(arr[i]["text"]) + "</a>";
			}
			break;
			
			case "span": {
				str+="<span>" + escapeHTML(arr[i]["text"]) + "</span>";
			}
			break;
			
			case "text": {
				str+=arr[i]["text"];
			}
			
		}
		
	}
	
	return str;
	
}

function objectToString(arr){
	
	var str="";
	
	for(var i = 0; i < arr.length; i++) {
		
		str+=arr[i]["text"];
		
	}
	
	return str;
	
}

function nodeToObject(content_node){
		
	var content_node_length = content_node.childNodes.length;

	var arr =[];

	for(var i = 0; i < content_node_length; i++) {
		
		var store_obj;
		
		switch(content_node.childNodes[i].nodeType) {
			
			case Node.TEXT_NODE: {
				
				var value_text = content_node.childNodes[i].textContent;
				
					store_obj = {
						"type": "text",
						"text": content_node.childNodes[i].textContent
					};
					arr.push(store_obj);
				
			} break;
			
			case Node.ELEMENT_NODE: {
				
				var tagName = content_node.childNodes[i].tagName.toLowerCase();
				
				switch(tagName) {
					
					case "div": {
						store_obj = {
							"type": "div",
							"text": content_node.childNodes[i].textContent
						};
						arr.push(store_obj);
					} break;
					
					case "span": {
						store_obj = {
							"type": "span",
							"text": content_node.childNodes[i].textContent
						};
						arr.push(store_obj);
					} break;
					
					case "a": {
						store_obj = {
							"type": "a",
							"text": content_node.childNodes[i].textContent,
							"href": content_node.childNodes[i].getAttribute("href")
						};
						
						arr.push(store_obj);
					} break;
				}
				
			} break;

		}
		
	}

	return arr;
	
}


function nodeToArray(content_node){
		
	var content_node_length = content_node.childNodes.length;

	var arr =[];

	for(var i = 0; i < content_node_length; i++) {
		
		var store_obj;
		
		switch (content_node.childNodes[i].nodeType) {
			
			case Node.TEXT_NODE: {
				
				var value_text = content_node.childNodes[i].textContent;
				
					store_obj =[];
					store_obj["type"] = "text";
					store_obj["text"] = content_node.childNodes[i].textContent;

					arr.push(store_obj);
				
			} break;
			
			case Node.ELEMENT_NODE: {
				
				var tagName = content_node.childNodes[i].tagName.toLowerCase();
				
				switch(tagName) {
					
					case "span": {
						store_obj =[];
						store_obj["type"] = "span";
						store_obj["text"] = content_node.childNodes[i].textContent;
						arr.push(store_obj);
					} break;
					
					case "a": {
						store_obj =[];
						store_obj["type"] = "a";
						store_obj["text"] = content_node.childNodes[i].textContent;
						store_obj["href"] = content_node.childNodes[i].getAttribute("href");
						
						arr.push(store_obj);
					} break;
					
				}
				
			} break;
			
		}
		
	}

	return arr;
	
}

function xhrUpdateNoteHandler(e){
	
	if (this.responseText!="") {
		
		try {

			var obj = JSON.parse(this.responseText);
		
			if (obj!= undefined && obj["action"]){
				
				switch(obj["action"]) {
					
					case "updateNote": {
						
						var id = document.getElementById(obj["body"]["id"]);
								
						storage.get(id);

						var title_note = id.children[0].firstElementChild;
						var content_note = id.children[1].firstElementChild;
						
						var title_value = obj["body"]["title"];
						var content_value = obj["body"]["note"];
						
						title_note.innerHTML = title_value;
						content_note.innerHTML = content_value;
						
						title_note.setAttribute("contenteditable","false");
						content_note.setAttribute("contenteditable","false");

						
						id.getElementsByClassName("menu")[0].innerHTML =
						"<img src=\"icons/edit_note_0.png\" class=\"edit_note\">"+
						"<img src=\"icons/delete_note_0.png\" class=\"delete_note\">";

						
					}
					
					break;
					
				}
				
			}
			
		} catch (err) {
			
			var el = document.createTextNode(this.responseText);
			output.appendChild(el);
			
		}

	}

}

function xhrDelNoteHandler(e){

	if (this.responseText!="") {
		
		try {

			var newstr = replaceAll("\n", "\\n", this.responseText);
			var obj = JSON.parse(newstr);
		
			if (obj!= undefined && obj["action"]) {
				
				switch(obj["action"]) {
					
					case "deleteNote": {
						var del = document.getElementById(obj["body"]["id"]);
						var parent = del.parentElement;
						parent.removeChild(del);
						
					}
					
					break;
					
				}
				
			}
			
		} catch (err) {
			
			var el = document.createTextNode(this.responseText);
			output.appendChild(el);
			
		}

	}
	
}

function addNoteHandler(e){ 

	e.stopPropagation();
	e.preventDefault();
	
	var form = document.forms["input_form"];
	
	var title = form["title"].value;
	var note = form["note"].value;
	
	title = replaceAll("\r", "", title);
	note = replaceAll("\r", "", note);
	
	var dynamicForm = new FormData();
	
	dynamicForm.append("action","addNote");
	
	dynamicForm.append("title", title);
	dynamicForm.append("note", note);
	
	postForm(dynamicForm, xhrAddNoteHandler);
	
}

function xhrAddNoteHandler(e){
	
	var output  = document.querySelector(".notes");

		if (this.responseText!="") {
		try {

			var obj = JSON.parse(this.responseText);

			if (obj!= undefined && obj["action"]){
				switch(obj["action"]) {
					
					case "addNote": {

						var str =	
							'<div id="'+obj["body"]["id"]+'" class="note">\
								<div class="title_note"><h2 class="title">'+
								escapeHTML(obj["body"]["title"])+'</h2>\
								<div class="menu">\
								<img src="icons/edit_note_0.png" class="edit_note"><img src="icons/delete_note_0.png" class="delete_note">\
								</div>\
								</div>\
								<div class="content_note">\
								<div class="body_note">'+
									obj["body"]["note"]+'</div>\
								</div>\
							</div>';
							
						var el = document.createElement("div");
						el.innerHTML= str;
						el = el.firstElementChild;
						
						el.addEventListener("mouseover", note_mouseover, false);
						el.addEventListener("mouseout", note_mouseout, false);
						el.addEventListener("click", note_click, false);
						//output.appendChild(el);
						
						output.insertBefore(el, output.firstElementChild);
					}
					
					break;
					
				}
			}
		} catch (err) {
			var el = document.createTextNode(this.responseText);
			output.appendChild(el);
		}
	}
	
}

function delAllNotesHandler(e){
	
	e.stopPropagation();
	e.preventDefault();
	
	var dynamicForm = new FormData();
	dynamicForm.append("action","deleteAllNotes");
	postForm(dynamicForm, xhrDelAllNotesHandler);
	
}

function xhrDelAllNotesHandler(e){
	
	var output  = document.getElementsByClassName("notes")[0];
	output.innerHTML="";
	
}

function postForm(form, handler){
	
	var xhr = new XMLHttpRequest();
	xhr.open("POST", window.location);
	xhr.addEventListener("load", handler, false);
	xhr.send(form);
	
}

function replaceAll(find, replace, str) {
	
	return str.replace(new RegExp(find, 'g'), replace);
	
}

var escape = document.createElement('textarea');

function escapeHTML(html) {
	
	escape.textContent = html;
	return escape.innerHTML;
	
}

function unescapeHTML(html) {
	
	escape.innerHTML = html;
	return escape.textContent;
	
}
