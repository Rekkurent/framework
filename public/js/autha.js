window.addEventListener("load", loadHandler, false);

function loadHandler(){ 

	var forms = document.forms["auth"];
	var send = forms["send"];

	send.addEventListener("click", authSendHandler, false);

}


function authSendHandler(e) {
	
	e.stopPropagation();
	e.preventDefault();
	
	var dynamicForm = new FormData();
	var forms = document.forms["auth"];
	
	var login = forms["login"].value;
	var password = forms["password"].value;
	
	dynamicForm.append("action","auth");
	dynamicForm.append("login", login);
	dynamicForm.append("password", password);
	
	postForm(dynamicForm, xhrSendHandler);

}

function xhrSendHandler(e) {

	if (this.responseText!="") {
		
		try {
			
			var obj = JSON.parse(this.responseText);

			if (obj!= undefined && obj["action"]) {
				
				switch(obj["action"]) {
					
					case "auth": { 
					
						if (
							obj["body"]["login"] == false || 
							obj["body"]["password"] == false
						) {
							
							var str ="Неверный логин или пароль";
							var el = document.getElementById("info");
							el.innerHTML = str;

						} else {
							
							location.replace('/index.php');
							
						}
						//authCancelHandler();
						//content.appendChild(el);
					}
					
				}
				
			}
			
		} catch(err) {
			
		}
		
	}
	
}

function authCancelHandler(){
	
	var elem = document.getElementById("auth");
	var parent = elem.parentElement;
	parent.removeChild(elem);

}


function postForm(form, handler){
	
	var xhr = new XMLHttpRequest();
	xhr.open("POST", '/auth.php');
	xhr.addEventListener("load", handler, false);
	xhr.send(form);
	
}
