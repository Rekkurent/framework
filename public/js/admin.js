
window.addEventListener("load", loadHandler, false);

function loadHandler(){

	var button_add_user = document.getElementById("ajax_add_user");
	button_add_user.addEventListener("click", add_user, false);
	
	var arr = document.getElementsByClassName("user");
	
	for (var i = 0; i < arr.length; i++) {
		
		arr[i].addEventListener("mouseover", user_mouseover, false);
		arr[i].addEventListener("mouseout", user_mouseout, false);
		arr[i].addEventListener("click", user_click, false);
		
	}
	
}


function add_user(e) {
	
	e.stopPropagation();
	e.preventDefault();
	
	var form = document.forms["add_user"];
	
	var login = form["add_login"];
	var password = form["add_password"];
	var type_user = form["user_type"];
	type_user = type_user.options[type_user.selectedIndex];
	
	var dynamicForm = new FormData();
	
	dynamicForm.append("action","addUser");
	
	dynamicForm.append("add_login", login.value);
	dynamicForm.append("add_password", password.value);
	dynamicForm.append("user_type", type_user.value);

	postForm(dynamicForm, xhrAddUserHandler);
	
}

function xhrAddUserHandler() {
		
	var output  = document.getElementById("form_view").firstElementChild;

	if (this.responseText!="") {
		
		try {

			//var newstr = replaceAll("\r\n", "\\n", this.responseText);
			var obj = JSON.parse(this.responseText);
			//obj["body"]["note"] = replaceAll("\n", "<br>", obj["body"]["note"]);
			
			if (obj!= undefined && obj["action"]) {
				
				switch(obj["action"]) {
					
					case "addUser": {

						var str =	'\
							<tr id="'+obj["body"]["id"]+'" class="user">\
								<td class="login_user">'+obj["body"]["login"]+'</td>\
								<td class="password_user">'+obj["body"]["password"]+'</td>\
								<td class="type_user">'+obj["body"]["type"]+'</td>\
								<td class="menu">\
								<img src="icons/delete_note_0.png" class="delete_user">\
								</td>\
							</tr>';
							
						var el = document.createElement("tbody");
						el.innerHTML= str;
						el = el.firstElementChild;
						el.addEventListener("mouseover", user_mouseover, false);
						el.addEventListener("mouseout", user_mouseout, false);
						el.addEventListener("click", user_click, false);
						output.lastElementChild.appendChild(el);
						//output.parentElement.appendChild(el);
					}
					
					break;
					
				}
				
			}
			
		} catch (err) {
			
			var el = document.createElement("div");
			el.innerHTML = this.responseText;
			
			output.parentElement.appendChild(el);
			
		}
		
	}
	
}


function user_mouseover(event) {
	
	var arr = {
		"delete_user" : "icons/delete_note_1.png",
		"edit_user" : "icons/edit_note_1.png",
		"cancel_user" : "icons/cancel_note_1.png",
		"save_user" : "icons/save_note_1.png"
	};
	
	var key = event.target.className;
	
	if (key in arr) {
		event.target.src = arr[key];
	}
	
}

function user_mouseout(event) {
	
	var arr = {
		"delete_user" : "icons/delete_note_0.png",
		"edit_user" : "icons/edit_note_0.png",
		"cancel_user" : "icons/cancel_note_0.png",
		"save_user" : "icons/save_note_0.png"
	};
	
	var key = event.target.className;
	
	if (key in arr) {
		event.target.src = arr[key];
	}
	
	
}


function user_click(event) {
	
	if (event.target.className == "delete_user") {
		
		event.stopPropagation();
		event.preventDefault();
		
		var dynamicForm = new FormData();
		
		dynamicForm.append("action","deleteUser");
		dynamicForm.append("id", event.currentTarget.id);
		
		postForm(dynamicForm, xhrDelUserHandler);
		
	} 
	
}

function xhrDelUserHandler(e) {

	if (this.responseText!="") {
		
		try {

			var newstr = replaceAll("\r\n", "\\n", this.responseText);
			var obj = JSON.parse(newstr);
		
			if (obj!= undefined && obj["action"]) {
				
				switch(obj["action"]) {
					
					case "deleteUser": {
						var del = document.getElementById(obj["body"]["id"]);
						var parent = del.parentElement;
						parent.removeChild(del);
						
					}
					
					break;
					
				}
				
			}
			
		} catch (err) {
			var el = document.createTextNode(this.responseText);
			output.appendChild(el);
		}

	}
	
}

function replaceAll(find, replace, str) {
	
  return str.replace(new RegExp(find, 'g'), replace);
  
}

function postForm(form, handler) {
	
	var xhr = new XMLHttpRequest();
	xhr.open("POST", "users.php");
	xhr.addEventListener("load", handler, false);
	xhr.send(form);
	
}

