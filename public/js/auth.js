window.addEventListener("load", loadHandler, false);

function loadHandler() { 

	var menu = document.getElementsByClassName("horizontal_menu")[0];
	var link = menu.lastElementChild;
	
	if (link.getAttribute("href") == "auth.php") {
		
		link.addEventListener("click", authHandler, false);
		
	}
	
}

function authHandler(event){ 

	event.stopPropagation();
	event.preventDefault();
	
	switch(event.which){
		//левая кнопка
		case 1:  { 
		
			var string = '\
				<div class="Absolute-Center">\
					<form name="auth">\
						<h2>Вход</h2>\
						<label for="login">Логин</label>\
						<input type="text" name="login" id="login">\
						<label for="password">Пароль</label>\
						<input type="password" name="password" id="password">\
						<span id="info"></span><br>\
						<input type="submit" value="Отправить" id="send">\
						<input type="reset" value="Отменить" id="reset">\
					</form>\
				</div>';
				
			var elem = document.createElement("div");
			elem.innerHTML = string;
			elem.setAttribute("id","auth");
			var inputs = elem.getElementsByTagName("input");
			
			for (var i = 0; i < inputs.length; i++) {

				switch (inputs[i].getAttribute("id")) {
					
					case "send" : {
						var send = inputs[i];
					} break;
					
					case "reset" : {
						var reset = inputs[i];
					} break;

				}
				
			}
			
			send.addEventListener("click", authSendHandler, false);
			reset.addEventListener("click", authCancelHandler, false);

			document.body.appendChild(elem);
			
		} break;
		
		//средняя кнопка
		case 2: {
			
			window.open("auth.php", "_blank");
			
		} break;
		
	}
	
}	

function authSendHandler(e) {
	
	e.stopPropagation();
	e.preventDefault();
	
	var dynamicForm = new FormData();
	var forms = document.forms["auth"];
	
	var login = forms["login"].value;
	var password = forms["password"].value;
	
	dynamicForm.append("action","auth");
	dynamicForm.append("login", login);
	dynamicForm.append("password", password);
	
	postForm(dynamicForm, xhrSendHandler);

}

function xhrSendHandler(e) {

	if (this.responseText!="") {
		
		try {
			
			var obj = JSON.parse(this.responseText);

			if (obj!= undefined && obj["action"]) {
				
				switch(obj["action"]) {
					
					case "auth": { 
					
						if (
							obj["body"]["login"] == false || 
							obj["body"]["password"] == false
						) {
							
							var str ="Неверный логин или пароль";
							var el = document.getElementById("info");
							el.innerHTML = str;

						} else {
							
							location.replace(location.href);
							
						}

						//authCancelHandler();
						//content.appendChild(el);
					}
					
				}
				
			}
			
		} catch(err) {
			
		}
		
	}
	
}

function authCancelHandler(){
	
	var elem = document.getElementById("auth");
	var parent = elem.parentElement;
	parent.removeChild(elem);

}


function postForm(form, handler){
	
	var xhr = new XMLHttpRequest();
	xhr.open("POST", "/auth.php");
	xhr.addEventListener("load", handler, false);
	xhr.send(form);
	
}
