<?php

namespace core {
	
	class Model {
		
		protected $db;
		protected $info;
		protected $user;
		protected $request;
		
		public function __construct($db, $user, $request) {
			
			$this->db = $db;
			$this->user = $user;
			$this->info = $user->getInfo();
			$this->request = $request;
			
		}
		
		public function isAuth() { 
		
			return $this->user->isAuth(); 
			
		}
		
		public function getUserTypeName() {
			
			$user_type = $this->info['user_type'];
			
			return $this->info->getUserTypeName($user_type);
			
		}
		
		public function get_current_page() {
			
			$current_page = $_SERVER['PHP_SELF']; 
			$current_page_array = explode( '/', $current_page);
			$current_page = $current_page_array[count($current_page_array)-1];
			$page = explode( '?', $current_page );
			$current_page = $page[0];
			
			return $current_page;
			
		}
		
		public function setAuth() {
			
			$login = $this->request['login'];
			$password = $this->request['password'];
			
			$this->user->setAuth($login, $password);
			
		}
		
		public function exitAuth() {
			
			$this->info->clearSession();
			
		}
		
		public function getResourceName() {
			
			return $this->request->getResourceName();
			
		}
		
	}
	
}

?>