<?php

namespace core {
	
	class Router {
		
		protected $request;
		protected $info;
		
		protected $routes = [
			"controller" => "",
			"model" => "",
			"view" => "",
			"template" => ""
		];
		
		public function  getRoutes() {
			
			$results = [];
			
			$resource_name  = $this->request->getResourceName();
			$this->info['auth'];
			$userType = $this->info['user_type'] ;
			$userTypeName = $this->info->getUserTypeName($userType);
			
			$path = 'context'. '\\'. $userTypeName;
			$base_path = 'context'. '\\'. 'user';
		
			foreach( $this->routes as $key => $value ) {
				
				$route =  DIRECTORY_SEPARATOR . ucfirst($key) . 's' . DIRECTORY_SEPARATOR . ucfirst($resource_name) . ucfirst($key);
				$route  = str_replace('/', '\\', $route);
				
				$file='/../application/'.$path . $route.'.php';
				$file = str_replace('\\', '/', $file);
				$file = $_SERVER['DOCUMENT_ROOT'].$file;
				//$route=$file;
				
				if ( file_exists ($file ) ) {
					
					$route = $path . $route;
					
				} else {
					
					$route = $base_path . $route;
					
				}

				$this->routes[$key] = $route;
				
			}

			$results = $this->routes;
			
			return $results;
			
		}
		
		public function __construct($request, $info) {
			
			$this->request = $request;
			$this->info = $info;
			
		}
		
	}
	
}

?>