<?php

namespace core {
	
	class View {
		
		protected $data = [];
		
		public function setData($data) {
			
			$this->data = $data;
			
		}
		
		public function show() {
			
			print_r($this->data);
			
		}
		
	}
	
}

?>