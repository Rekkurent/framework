<?php

namespace core {
	
	class ViewJSON extends View {
		
		public function show() {

			header('Content-Type: application/json');
			echo json_encode($this->data);
			
		}
		
	}
	
}

?>