<?php

namespace core {
	
	class ViewHTML extends View {
		
		protected $title = [];
		protected $styles = [];
		protected $scripts = [];
		protected $menu = [];
		
		protected $templates =[];
		
		protected $currentResource;
		
		public function setData($data) {
			
			parent::setData($data);
			
			if ( $this->data['isAuth'] == false ) {
				
				$this->menu ['Вход'] = 'auth.php';
				
			} else {

				$this->menu["Файлы"] = "files.php";
				
				if(
					isset($this->data['userTypeName']) &&
					$this->data['userTypeName'] == 'admin'
				){
					$this->menu ['Администрирование'] = 'admin.php';
				}
				
				$this->menu ['Выход'] = 'auth.php?action=exit';
				
			}
			
		}
		
		public function __construct() {
			
			$this->menu = [
			
				"Главная" => "index.php",
				"Ссылки" => "links.php",
				"Заметки" => "notes.php"
				
			];
			
			$this->currentResource = ltrim($_SERVER['PHP_SELF'], '/'); 
			
		}

		
		public function showMenu($menu, $style, $current_element) {
			
			$menu_str = "";
			
			$menu_str .= "<div class=\"".$style."\">";
			
			foreach($menu as $key=>$value ) {
				
				if($value == $current_element) {
					
					$menu_str .= "<a>$key</a>";
					
				} else {
					
					$menu_str .=  "<a href=\"$value\">$key</a>";
					
				}
				
			}
			
			$menu_str .= "</div>";
			
			echo $menu_str;
			
			
		}
		
		
		
		public function setTemplate($name, $template) {
			
			$this->templates[$name] = $template;
			
		}

		
		public function showTemplate($name) {
			
			if (isset($this->templates[$name])) {
				
				require_once($this->templates[$name]);
				
			}
			
		}

		protected function addScripts($scripts) {
			
			$this->scripts = array_merge($this->scripts, $scripts);
			
		}
		
		protected function addStyles($styles) {

			$this->styles = array_merge($this->styles, $styles);
			
		}
		
		protected function showScripts() {
			
			foreach ($this->scripts as $value) {
				
				echo '<script type="text/javascript" src="' . $value . '"></script>';
				
			}
			
		}
		
		protected function showStyles() {
			
			foreach ($this->styles as $value) {
				
				echo '<link rel="stylesheet" type="text/css" href="' . $value . '">';
				
			}
			
		}
		
		protected function showTitle() {
			
			echo '<title>' . $this->title . '</title>';
			
		}
		
		
		protected function showBody() {
			
			echo "<pre>";
			print_r($this->data);
			print_r($this->menu);
			print_r($this->currentResource); 
			echo "</pre>";
			
		}
		
		public function show() {
			
			$this->showTemplate('main');

		}

	}
	
}

?>