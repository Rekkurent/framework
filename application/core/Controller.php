<?php

namespace core {
	
	use tools\Debug;
	use core\ViewJSON;
	
	class Controller {
		
		protected $model;
		protected $view;
		protected $data = [];
		
		public function __construct($model, $view) {
			
			$this->model = $model;
			$this->view = $view;
			
		}
		
		public function actionDefault() {
			
			$debug = Debug::getInstance();
			$messages = $debug->getAllMessages();
				
			$debug_data = [
				'test_view_key' => 'test_view_value',
				'debug_messages' => $messages
			];
			
			$current_element = $this->model ->getResourceName();
			$current_page = $this->model ->get_current_page();
			$this->data = [
				'debug' => $debug_data,
				'current_element' =>	$current_element 
			];
			
			$this->data['userTypeName'] = $this->model->getUserTypeName();
			$this->data['isAuth'] = $this->model->isAuth();
			
			$this->view->setData($this->data);
			$this->view->show();
			
		}
		
		public function authAction() {

			$this->model->setAuth();
		
			$isAuth = $this->model->isAuth();
			$this->data['action'] = 'auth';
			$this->data["body"]["login"] =true;
			$this->data["body"]["password"]  =true;
			
			$this->view = new ViewJSON();
			$this->view->setData($this->data);
			$this->view->show();
		}
		
	}
	
}

?>