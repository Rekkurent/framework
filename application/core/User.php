<?php

namespace core {

	class User {
		
		protected $db;
		protected $info;

		public function __construct($db, $info) {
			
			$this->db = $db;
			$this->info = $info;
			
		}
		
		public function isAuth() {
			
			return $this->info['auth'];
			
		}
		
		public function getInfo() {
			
			return $this->info;
			
		}
		
		public function exitAuth(){
			$this->info->clearSession();
		}
		
		public function setAuth($login, $password) {
			
			$select_users = "
				SELECT 
					* 
				FROM 
					users
				WHERE
					users.login = :login
				AND
					users.password = sha(:password)";
					
			$stmt = $this->db->prepare($select_users);
			 
			$params = [
				'login' => $login,
				'password' => $password
			];
			
			$stmt->execute($params);
			
			$rows =[];
			
			if ($stmt->rowCount() > 0) {
				
				$row = $stmt->fetch();
				
				$this->info['auth'] = true;
				$this->info['id_user'] = $row['id_user'];
				$this->info['login'] = $row['login'];
				$this->info['user_type'] = $row['user_type'];
				
			}
			
		}
		
	}
	
}

?>