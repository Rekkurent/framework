<?php

namespace tools {
	use \PDO;
	class DB {
		
		protected $pdo;
		protected $host = '';
		protected $db   = '';
		protected $user = '';
		protected $password = '';
		protected $charset = 'utf8';	
		protected $dsn;	
		protected $options;
		
		public function __construct($config) {

			$this->host = $config['host'];
			$this->db   = $config['db'];
			$this->user = $config['user'];
			$this->password = $config['password'];
		
			$this->dsn = 
				"mysql:host=".	$this->host.
				";dbname=".$this->db.
				";charset=".$this->charset;
			
			$this->options = [
				PDO::ATTR_ERRMODE	=> PDO::ERRMODE_EXCEPTION,
				PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
				PDO::ATTR_EMULATE_PREPARES   => false,
			];
			
			$this->pdo = new PDO(
				$this->dsn, 
				$this->user, 
				$this->password, 
				$this->options
			);
		
		}
		
		public function getDB() {
			
			return $this->pdo;
			
		}

		
	}
	
}

?>