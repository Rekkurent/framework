<?php

namespace tools {
	
	class Debug {
		
		protected static $instance;
		protected $data = [];
		
		public static function getInstance() {
			
			if ( empty ( self::$instance ) ) {
				
				self::$instance = new Debug();
				
			}
			
			return self::$instance;
			
		}
		
		protected function __construct() {
			
		}
		
		public function getMessage($key) {
			
			return $this->data[$key];
			
		}
		
		public function getAllMessages() {
			
			return $this->data;
			
		}
		
		
		public function setMessage($key, $value) {
			
			$this->data[$key] = $value;
			
		}
		
	}
	
}

?>