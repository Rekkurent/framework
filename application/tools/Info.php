<?php

namespace tools {
	
	use \ArrayAccess;
	use \Iterator;
	
	class Info implements ArrayAccess, Iterator {
		
		//ArrayAccess
		//Iterator
		
		protected $data = [];
		protected $config = [];
		
		protected $user_types = [
			0 =>"admin",
			2 =>"user"		
		];
		
		public function getUserTypeName($key) {
			
			$key = (!isset($key)) ? 2 : $key;
			
			//print_r($key);
			
			return $this->user_types[$key];
			
		}
		
		public function __construct($config) {
			
			@session_start();
			
			$this->data = $config;

			$this->config = array_merge([], $this->data );
			
			foreach($_SESSION as $key => $value) {
				
				$this->data[$key] = $value;
				
			}
			
		}
			
		public function clearSession(){
			
			foreach($_SESSION as $key => $value) {
				
				unset($_SESSION[$key]); 
				
			}
			
			$this->data  =  array_merge([], $this->config); 
			
		}
		
		
		
		/*	
			Iterator
				rewind
				current
				key
				next
				valid
		*/	
			
		public function rewind() {
			
			reset($this->data);
			
		}

		public function current() {
			
			return current($this->data);
			
		}

		public function key() {
			
			return key($this->data);
			
		}

		public function next() {
			
			next($this->data);
			
		}

		public function valid() {
			
			return array_key_exists( $this->key(), $this->data);
			
		}
		
		
		
		/*	
			ArrayAccess
				offsetSet
				offsetExists
				offsetUnset
				offsetGet
		*/	

		public function offsetSet($offset, $value) {

			$this->data[$offset] = $value;
			$_SESSION[$offset] = $value;

		}

		public function offsetExists($offset) {
			
			return array_key_exists($offset, $this->data);
			
		}

		public function offsetUnset($offset) {
			
			unset($this->data[$offset]);
			unset($_SESSION[$offset]); 
			
		}

		public function offsetGet($offset) {
			
			return array_key_exists($offset, $this->data) ? $this->data[$offset] : null;
			
		}
		
	}
	
}

?>