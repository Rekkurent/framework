<?php

namespace tools {
	
	class ConfigDB {
		
		protected $configs = [
			'framework.my' => [
				'host' => 'localhost',
				'db'   => 'notes',
				'user' => 'root',
				'password' => 'YES'
			],
			'smartnotes.h1n.ru' => [
				'host' => 'localhost',
				'db'   => 'notes',
				'user' => 'note0',
				'password' =>'pass4me0'
			]
		];
		
		protected $config = [];
		
		public function __construct(){
			
			$server_name = $_SERVER['SERVER_NAME'];

			if ( array_key_exists($server_name, $this->configs) ) {
				
				$this->config = $this->configs[$server_name];
				
			}
			
		}
		
		public function getConfig(){
			
			return $this->config;
			
		}
		
	}
	
}

?>