<?php

namespace tools {
	
	use \ArrayAccess;
	use \Iterator;
	
	class RequestBase implements ArrayAccess, Iterator {
		
		//ArrayAccess
		//Iterator
		protected $data = [];
		
		public function __construct($config) {

			$this->data = array_merge([], $config);
			
		}
		
		/*	
			Iterator
				rewind
				current
				key
				next
				valid
		*/	
			
		public function rewind() {
			
			reset($this->data);
			
		}

		public function current() {
			
			return current($this->data);
			
		}

		public function key() {
			
			return key($this->data);
			
		}

		public function next() {
			
			next($this->data);
			
		}

		public function valid() {
			
			return array_key_exists( $this->key(), $this->data);
			
		}
		
		/*	
			ArrayAccess
				offsetSet
				offsetExists
				offsetUnset
				offsetGet
		*/	

		public function offsetSet($offset, $value) {

			$this->data[$offset] = $value;

		}

		public function offsetExists($offset) {
			
			return array_key_exists($offset, $this->data);
			
		}

		public function offsetUnset($offset) {
			
			unset($this->data[$offset]);
			
		}

		public function offsetGet($offset) {
			
			return array_key_exists($offset, $this->data) ? $this->data[$offset] : null;
			
		}
		
	}
	
}

?>