<?php

namespace tools {
	
	class Request extends RequestBase {

		protected $data = [];
		
		protected $resource_name = '';
		
		public function getResourceName() {
			
			return $this->resource_name;
			
		}
		
		public function getContentType() { 
		
		
		
		}
		
		public function __construct() {

			parent::__construct($_REQUEST);
			
			$resource_name = $_SERVER['PHP_SELF'];
			
			$this->resource_name  = basename($resource_name , ".php");

		}
		
	}

}

?>