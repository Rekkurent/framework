<?php

use core\User;
use core\Router;

use tools\Request;
use tools\Info;
use tools\Debug;

use tools\DB;
use tools\ConfigDB;


class Application {
	
	public function init() {
		
		$config = [
			'auth' => false,
			'id_user' => null,
			'login' => '',
			'user_type' => null
		];

		$info = new Info($config);
		
		$request = new Request();
		
		$router = new Router($request, $info);
		$routes_path = $router->getRoutes();
		
		$configDB = new ConfigDB();
		$config = $configDB->getConfig();
		$db = new DB($config);
		$pdo = $db->getDB();
		$user = new User($pdo, $info);
		
		$action = isset( $request['action'] ) ? $request['action'] ."Action" : 'actionDefault';
	
		$model = new $routes_path['model']($pdo, $user, $request);
		$view = new $routes_path['view']();
		$controller = new $routes_path['controller']($model, $view);
		
		$debug = Debug::getInstance();
		
		$debug_info = [
			'routes_path' => $routes_path 
		];
	
		$debug->setMessage("debug", $debug_info);
		$debug->setMessage("info", $info);

		//ReflectionObject
		$rc = new ReflectionClass($controller);

		if ( $rc->hasMethod($action) ) {
			
			$controller->$action();
			
		}

	}
	
}

?>