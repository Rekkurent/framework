<?php

namespace context\admin\Views {
	
	use context\user\Views\MainView;
	
	class AdminView extends MainView {
		
		protected $vertical_menu = [];
		
		public function __construct() {
			
			parent::__construct();
			
			$this->title = "Администрирование";
			
			$template = 'context/admin/Templates/adminTemplate.php';
			$this->setTemplate('index', $template);
			
			$main_template = 'context/admin/Templates/mainTemplate.php';
			$this->setTemplate('main', $main_template);
			
			$this->vertical_menu = [
				"Пользователи" => "users.php"
			];

			$styles = [
				'/css/admin.css',
				'/css/type_admin.css'
			];

			$this->addStyles($styles);

		}
		
	}
	
}

?>