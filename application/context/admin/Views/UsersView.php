<?php

namespace context\admin\Views {
	
	use context\user\Views\MainView;
	
	class UsersView extends MainView {
		
		protected $vertical_menu = [];
		
		public function __construct() {
			
			parent::__construct();
			
			$this->title = "Пользователи";
			
			$template = 'context/admin/Templates/usersTemplate.php';
			$this->setTemplate('users', $template);
			
			$main_template = 'context/admin/Templates/mainTemplate.php';
			$this->setTemplate('main', $main_template);
			
			$this->vertical_menu = [
				"Пользователи" => "users.php"
			];
			
			$scripts = [
				'/js/admin.js'
			];

			$styles = [
				'/css/admin.css',
				'/css/type_admin.css'
			];
			
			$this->addScripts($scripts);
			$this->addStyles($styles);

		}
		
		protected function showBody() {

			$this->showTemplate('users');
			
		}
		
		public function showUsersList() {

			$descrText = ["Логин", "Пароль", "Уровень доступа",""];
			
			$th_str ='';
			
			foreach ($descrText as $value) {
				
				$th_str .= '<th>'.$value.'</th>';
				
			}
			
			
			$users = $this->data['usersList'];
			
			$tr_str = '';
			
			for ($i = 0; $i < count($users); $i++) {
				
				$user = $users[$i];
				
				$str = '
					<tr id="' . $user["id_user"] . '" class="user">
						<td class="login_user">' . $user["login"] . '</td>
						<td class="password_user">' . $user["password"] . '</td>
						<td class="type_user">' . $user["user_type_name"] . '</td>
						<td class="menu">
						<img src="icons/delete_note_0.png" class="delete_user">
						</td>
					</tr>';
					
				$tr_str .= $str;
				
			}

			$result = '
				<form name="view_user"  id="form_view" >
					<table>
						<caption>Пользователи</caption>
						<tbody>
							<tr>
								' . $th_str . '
							</tr>
							' . $tr_str . '
						</tbody>
					</table>
				</form>	
			';
			
			echo $result;
			
		}

	}
	
}

?>