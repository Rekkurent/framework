<?php

namespace context\admin\Controllers {
	
	use core\Controller;
	use core\ViewJSON;
	//use context\user\Controllers;
	
	class UsersController extends Controller {

		
		public function actionDefault() {
			
			$current_element = $this->model ->getResourceName();
			
			$this->data = [
				'current_element' =>	$current_element 
			];
			
			$this->data['userTypeName'] = $this->model->getUserTypeName();
			$this->data['isAuth'] = $this->model->isAuth();
			$this->data['usersList'] = $this->model->getUsersList();
			$this->view->setData($this->data);
			$this->view->show();
			
		}
		
		public function addUserAction() {
			
			$this->view = new ViewJSON();
			
			$this->data = $this->model->addUser();
			$this->view->setData($this->data);
			$this->view->show();
			
		}	
		
		public function deleteUserAction() {
			
			$this->view = new ViewJSON();
			
			$this->data = $this->model->deleteUser();
			$this->view->setData($this->data);
			$this->view->show();
			
		}	
		
	}
	
}

?>