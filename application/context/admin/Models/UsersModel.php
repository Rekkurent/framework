<?php

namespace context\admin\Models {
	
	use core\Model;
	//use context\user\Models;
	
	class UsersModel extends Model {
		
		public function getUserTypeNameLocal($type){
			
			$users_type = [
				0 => "Администратор",
				2 => "Пользователь"
			];
			
			return $users_type[$type];
			
		}
		
		public function getUsersList() {
			
			$query = "
				select 
					id_user, 
					login, 
					password, 
					user_type 
				from 
					users";
					
			$stmt = $this->db->prepare($query);
			
			$stmt->execute();

			
			$arr =[];
			
			if ($stmt->rowCount() > 0) {
				
				$arr = $stmt->fetchAll();
				
			}
			
			for ($i = 0; $i < count($arr); $i++) {
				
				$user_type = $arr[$i]['user_type'];
				$user_type_name = $this->getUserTypeNameLocal($user_type);
				$arr[$i]['user_type_name'] = $user_type_name;
				
			}

			return $arr;
			
		}
		
		public function addUser(){
			
			$login=$this->request["add_login"];
			$password=$this->request["add_password"];
			$user_type=$this->request["user_type"];
		
			$insert = "
				insert into 
					users 
				set 
					login=:login, 
					password=:password, 
					user_type=:user_type";
					
			$stmt = $this->db->prepare($insert);

			try { 
			
				$this->db->beginTransaction(); 
		
				$params = [
					'login' => $login,
					'password' => sha1($password),
					'user_type' => $user_type
				];
				
				$stmt->execute($params);
				$id_user =$this->db->lastInsertId(); 
				$this->db->commit(); 
				
			} catch(PDOExecption $e) { 
			
				$this->db->rollback(); 
				//print "Error!: " . $e->getMessage() . "</br>"; 
				
			} 

			$path = $_SERVER["DOCUMENT_ROOT"]."/../files/".$id_user;
			//$path = realpath($path);
			mkdir($path, 0777, true);
			
			$user_type_name = $this->getUserTypeNameLocal($user_type);

			$out = [	
				"action" =>"addUser",
				"body" => [
					"id" =>$id_user,
					"login" => $login,
					"password" =>sha1($password),
					"type" =>$user_type_name
				]
			];
			
			return $out;
			
		}
		
		public function deleteUser(){
			
			$id_user = $this->request['id'];
			
			$delete = "
				delete from 
					users 
				where 
					id_user=:id_user";
					
			$stmt = $this->db->prepare($delete);
			
			$params = [
				'id_user' => $id_user
			];
				
			$stmt->execute($params);
		
			$path = $_SERVER["DOCUMENT_ROOT"]."/../files/".$id_user;
			
			$this->rrmdir($path);

			$out = [	
				"action" =>"deleteUser",
				"body" => [
					"id" => $id_user
				]
			];
			
			return $out;
			
		}	
		
		public function rrmdir($path) {
			
			 // Open the source directory to read in files
			$i = new \DirectoryIterator($path);
			
			foreach ($i as $f) {
				
				if (
					$f->isFile()
				) {
					
					unlink($f->getRealPath());
					
				} else if (
					!$f->isDot() && 
					$f->isDir()
				) {
					
					$this->rrmdir($f->getRealPath());
					
				}
				
			}
			
			rmdir($path);
			
		}
		
	}

}

?>