<?php

namespace context\user\Controllers {
	
	use core\Controller;
	use tools\Debug;
	
	class AuthController extends Controller {

		public function exitAction() {

			$this->model->exitAuth();
			
			$debug = Debug::getInstance();
			$messages = $debug->getAllMessages();
				
			$debug_data = [
				'debug_messages' => $messages
			];
			
			$current_element = $this->model ->getResourceName();
			
			$this->data = [
				'debug' => $debug_data,
				'current_element' =>	$current_element 
			];
			
			$this->data['userTypeName'] = $this->model->getUserTypeName();
			$this->data['isAuth'] = $this->model->isAuth();
			$this->view->setData($this->data);
			$this->view->show();
			
		}
		
	}
	
}

?>