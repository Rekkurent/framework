<?php

namespace context\user\Controllers {
	
	use core\Controller;
	use core\ViewJSON;
	use core\ViewFile;
	
	class FilesController extends Controller {
		
		public function actionDefault() {
			
			$current_element = $this->model ->getResourceName();
			
			$this->data = [
				'current_element' =>	$current_element 
			];
			
			$this->data['userTypeName'] = $this->model->getUserTypeName();
			$this->data['isAuth'] = $this->model->isAuth();
			$this->data['files'] = $this->model->getFiles();
			$this->view->setData($this->data);
			$this->view->show();
			
		}
		
		public function getFileAction() {
			
			$this->view = new ViewFile();
			
			$this->data['file'] = $this->model->getFile();
			$this->view->setData($this->data);
			$this->view->show();
			
		}	
		
		public function uploadFilesAction(){
			
			$this->view = new ViewJSON();
			
			$this->data['info'] = $this->model->uploadedHandler();
			$this->data['files'] = $this->model->getFiles();
			
			$this->view->setData($this->data);
			$this->view->show();
			
		}
		
		public function deleteFileAction(){
			
			$this->view = new ViewJSON();
			
			$name = $this->model->deleteFile();
			
			$this->data = [	
				"action" =>"deleteFile",
				"body" => [
					"name" => $name
				]
			];
			
			$this->view->setData($this->data);
			$this->view->show();
			
		}

	}
	
}

?>