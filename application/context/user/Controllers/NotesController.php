<?php

namespace context\user\Controllers {
	
	use core\Controller;
	use core\ViewJSON;
	
	class NotesController extends Controller {
		
		public function actionDefault() {
			
			$current_element = $this->model ->getResourceName();
			
			$this->data = [
				'current_element' =>	$current_element 
			];
			
			$this->data['userTypeName'] = $this->model->getUserTypeName();
			$this->data['isAuth'] = $this->model->isAuth();
			//$notesInfo = $this->model->getNotes();
			//$this->data['notes'] = $notesInfo['notes'];
			$this->view->setData($this->data);
			$this->view->show();
			
		}
		
		public function addNoteAction() {
			
			$this->view = new ViewJSON();
			$this->data = $this->model->addNote();
			$this->view->setData($this->data);
			$this->view->show();
			
		}	
		

		
		public function deleteAllNotesAction() {
			
			$this->view = new ViewJSON();
			$this->data = $this->model->deleteAllNotes();
			$this->view->setData($this->data);
			$this->view->show();
			
		}
		
		public function getNotesAction() {
			
			$this->view = new ViewJSON();
			$this->data = $this->model->getNotes();
			$this->view->setData($this->data);
			$this->view->show();
			
		}
		
		
		public function deleteNoteAction() {
			
			$this->view = new ViewJSON();
			$this->data = $this->model->deleteNote();
			$this->view->setData($this->data);
			$this->view->show();
			
		}
		
		public function updateNoteAction() {
			
			$this->view = new ViewJSON();
			$this->data = $this->model->updateNote();
			$this->view->setData($this->data);
			$this->view->show();
			
		}

	}
	
}

?>