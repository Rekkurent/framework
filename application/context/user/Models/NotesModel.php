<?php

namespace context\user\Models {
	
	use core\Model;
	use tools\URL;
	
	class NotesModel extends Model {
		
		protected  $url_analyze;
		
		public function __construct($db, $user, $request) {
			
			parent:: __construct($db, $user, $request);
			$this ->url_analyze = new URL();
			
		}
		
		public function addNote() {
			
			$title = $this->request['title'];
			$content = $this->request["note"];
			
			$content = $this->url_analyze->Show($content);
			
			$insert ="
				insert into 
					notes 
				set 
					id_user=:id_user, 
					title=:title, 
					content=:content";
					
			$stmt = $this->db->prepare($insert);
			 
			$id_note = '';
			
			try { 
			
				$this->db->beginTransaction(); 
		
				$params = [
					'id_user' => $this->info['id_user'],
					'title' => $title,
					'content' => $content
				];
				
				$stmt->execute($params);
				$id_note =$this->db->lastInsertId(); 
				$this->db->commit(); 

			} catch(PDOExecption $e) { 
			
				$this->db->rollback(); 
				//print "Error!: " . $e->getMessage() . "</br>"; 
			} 

			$data = [
				"action" =>"addNote",
					"body" => [
						"id" => $id_note,
						"title" => $title,
						"note" =>$content
					]
			];
			
			return $data;
			
		}
		

		
		public function deleteAllNotes() {
			
			$delete ="
				delete from 
					notes 
				where 
					id_user=:id_user";
					
			$stmt = $this->db->prepare($delete);
			
			$params = [
				'id_user' => $this->info['id_user']
			];
				
			$stmt->execute($params);
			
			$out = [	
				"action" =>"deleteAllNotes"
			];
			
		}
		
		public function deleteNote() {
			
			$id_note = $this->request["id"];

			$delete = "
				delete from 
					notes 
				where 
					id_user=:id_user 
				and 
					id_note=:id_note";
					
			$stmt = $this->db->prepare($delete);

			try { 
			
				$this->db->beginTransaction(); 
		
				$params = [
					'id_user' => $this->info['id_user'],
					'id_note'=>$id_note
				];
				
				$stmt->execute($params);
				$this->db->commit(); 
				
				
			} catch(PDOExecption $e) { 
			
				$this->db->rollback(); 
				//print "Error!: " . $e->getMessage() . "</br>"; 
				
			} 
			
			$out = [	
				"action" =>"deleteNote",
				"body" => [
					"id" => $id_note
				]
			];
			
			return $out;
			
		}
		
		public function updateNote() {
			
			$id = $this->request["id"];
			$title = $this->request['title'];
			$content = $this->request["note"];
			$content = json_decode($content, true);
			$content = $this->objectToHtml($content);
					
			$update ="
				update 
					notes 
				set 
					title=:title, 
					content=:content 
				where 
					id_user=:id_user 
				and 
					id_note=:id_note";
					
			$stmt = $this->db->prepare($update);
			
			try { 
			
				$this->db->beginTransaction(); 
		
				$params = [
					'id_user' => $this->info['id_user'],
					'title' => $title,
					'content' => $content,
					'id_note'=>$id
				];
				
				$stmt->execute($params);
				$this->db->commit(); 
				
			} catch(PDOExecption $e) { 
			
				$this->db->rollback(); 
				//print "Error!: " . $e->getMessage() . "</br>"; 
				
			} 

			$out = [	
				"action" =>"updateNote",
				"body" => [
					"id" => $id,
					"title" => $title,
					"note" => $content
				]
			];
			
			return $out;
			
		}
		
		public function objectToHtml($content) {
			
			$str="";
			
			$url_analyze = $this ->url_analyze;
			
			foreach ($content as $key => $value) {
				
				switch ($content[$key]["type"]) {
					
					case "text": {
						
						$str.= $url_analyze->Show($content[$key]["text"]);
						
					} 
					
					break;
					
					case "div": {
						
						$str.="<div>".$url_analyze->Show($content[$key]["text"])."</div>";
						//$str.=$content[$key]["text"]."\r\n";
					} 
					
					break;
					
					case "span": {
						
						//$str.=$url_analyze->Show($content[$key]["text"]);
						$str.="<span>".$url_analyze->Show($content[$key]["text"])."</span>";
						
					} 
					
					break;
					
					case "a": {
						
						if (isset($content[$key]["href"])) {
							
							$href = "";
							$href = "href=\"".$url_analyze->ShowURL($content[$key]["href"])."\"";
							$str .= "<a ".$href.">".$content[$key]["text"]."</a>";
							
						} else {
							
							$str.=$url_analyze->Show($content[$key]["text"]);
							
						}
						
						
					} 
					
					break;
					
					case "img": {
						
					} 
					
					break;
				}
				
			}
			
			return $str;
			
		}	
	
		public function getNotes() {
			
			$id_note=$this->request['id_note'];
			$data = $this->getNotesByOffset($id_note);
			
			return $data;
			
		}
		
		public function getNotesCount(){
			
			$select = "
				select 
					count(*) as count
				from 
					notes 
				where 
					id_user=:id_user";
					
			$params = [
				'id_user' => $this->info['id_user']
			];	
			
			$stmt = $this->db->prepare($select);

			$stmt->execute($params);

			
			$arr = [];
			
			if ($stmt->rowCount() > 0) {
				
				$arr = $stmt->fetch();
				
			}
			
			return $arr['count'];	
			
		}
		
		
		public function getAllNotes(){
			
			$data = $this->getNotesByOffset();
			
			return $data;
			
		}
		
		public function getNotesByOffset($id_note=NULL) {
			
			
			$select = "
				select 
					id_note, 
					title, 
					content
				from 
					notes 
				where 
					id_user=:id_user";
					
		
			$select_2="
				order by
					id_note
						desc
				";	
				
			$params = [
				'id_user' => $this->info['id_user']
			];		
			
			$data = [
				'notes' => []
			];
			
			if (!is_null($id_note)) {
	
				$where="
					and
						id_note < :id_note
				";
				$select.=$where;
				
				
				if (
					!(
						isset($this->request['limit']) &&
						$this->request['limit'] =="all"
					)
				) {
					
					$select_last="
						limit :limit
					";
					
					$select_2 .= $select_last;
					
					
					$params['limit'] = 10;
					$data['limit'] = $params['limit'];
					
				} else {
					
					$data['limit']="all";
					
				}	

				$params['id_note'] = $id_note; 
				$data['id_note'] = $params['id_note'];

			} else {
				
				if (
					!(
						isset($this->request['limit']) &&
						$this->request['limit'] =="all"
					)
				) {
					$select_last="
						limit :limit
					";
					
					$select_2 .= $select_last;
					$params['limit'] = 10;
					$data['limit'] = $params['limit'];
				} else {
					$data['limit']="all";
				}	
				
			} 
			
			$select.=$select_2;
			
			$stmt = $this->db->prepare($select);
			$stmt->execute($params);
			
			$arr =[];
			
			if ($stmt->rowCount() > 0) {
				
				$arr = $stmt->fetchAll();
			}

			$data['notes'] = $arr;
			$data['select'] = $select;
			
			return $data;

		}
		
	}
	
}

?>