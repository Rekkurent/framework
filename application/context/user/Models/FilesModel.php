<?php

namespace context\user\Models {
	
	use core\Model;

	class FilesModel extends Model {

		public function getFiles(){
			
			$name = $_SERVER["DOCUMENT_ROOT"]."/../files/".$this->info['id_user'];
			//print_r($name);
			$info = [];
			
			$info = $this->getDirInfo($name);
			
			return $info;
			
		}
		
		public function getDirInfo($name){
			
			$files = [];
			
			if (!file_exists($name)) {
				mkdir($name, 0777);
			}
			
			$dir = dir($name);
			//$str = "Свободное место: ".disk_free_space("/")."<br>";
			//$str .="Всего места: ".disk_total_space("/")."<br>";
			//$str = "<div id=\"current_dir\"><p>Каталог: </p><ul>";
			
			while ( false !== ( $file = $dir->read() ) ) {
				
				if ($file != "." && $file != "..") {
					
					$files []= $file;
					
				}
				
			}
			
			//$str .= "</ul></div>";
			
			$dir->close();
			
			return $files;

		}
		
		public function getFile() {
			
			$path='';
			
			if (isset($this->request["name"])) {
				
				$file_name = $this->request["name"];
				$path = $_SERVER["DOCUMENT_ROOT"]."/../files/".$this->info['id_user']."/".$file_name;
				
			}
			
			return $path;
			
		}
		
		
		public function deleteFile() {
			
			$name = $this->request["name"];
			$path = $_SERVER["DOCUMENT_ROOT"]."/../files/".$this->info['id_user'];
			$full_name = $path."/".$name;
			unlink($full_name);
			return $name;
			
		}
		
		//обработка загруженных файлов
		public function uploadedHandler() {
			
			$files_count = count($_FILES);

			$str = "";
			$info = [];
			$path_base = $_SERVER["DOCUMENT_ROOT"]."/../files/".$this->info['id_user'];
			
			if ($files_count > 0) {
				//echo "Количество файлов: ".$files_count."<br>\n";
				
				foreach ($_FILES  as $key_elem =>$value_elem) { 
					//echo "Файл ".$key_elem."<br>\n"; // имя файла формы
					$str = '';
					
					if ($value_elem["error"] != UPLOAD_ERR_OK) {
						
						$str .="Файл ".$value_elem["name"].". Проблема: ";
						
						switch ($value_elem["error"]) {
							
							case UPLOAD_ERR_INI_SIZE: 
								$str .= "Размер файла больше upload_max_filesize";
							break;
							
							case UPLOAD_ERR_FROM_SIZE: 
								$str .= "Размер файла больше max_file_size";
							break;
							
							case UPLOAD_ERR_PARTIAL: 
								$str .= "Загружена только часть файла";
							break;
							
							case UPLOAD_ERR_NO_FILE: 
								$str .= "Файл не загружен";
							break;
							
							case UPLOAD_ERR_NO_TMP_DIR: 
								$str .= "Загрузка невозможна: не задан временный каталог";
							break;
							
							case UPLOAD_ERR_CANT_WRITE: 
								$str .= "Загрузка не выполненна: невозможна запись на диск";
							break;
						}
						
						$info []= $str;
						
					} else {
						
						$path = $path_base."/".$value_elem["name"];
						$str .= $path."<br>\n";
						
						if (is_uploaded_file($value_elem["tmp_name"])) {
							
							if (!move_uploaded_file($value_elem["tmp_name"], $path)) {
								
								$str .="Проблема: невозможно переместить файл в каталог назначения";
								$info[]= $str;
								
							}
							
						} else {
							
							$str .="Файл ".$value_elem["name"].".<br>\n";
							$str .= "Проблема: возможна атака через загрузку файла";
							$info[]= $str;
							
						}

					}
					
					/*
					$value_elem["name"];
					$value_elem["type"];
					$value_elem["tmp_name"];
					$value_elem["error"];
					$value_elem["size"];
					*/
					
				}
				
			}
			
			return $info;
			
		}

	}
	
}

?>