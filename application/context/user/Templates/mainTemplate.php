<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<?php 
			$this->showTitle();
			$this->showStyles();
			$this->showScripts();
		?>
	</head>
	<body>
		<div class="header">
			<h1>The Notebook</h1>
		</div>
		<?php 
			$current_element = $this->data['current_element'].".php";
			$this->showMenu($this->menu, "horizontal_menu", $current_element); 
		?>
		<div class="main">	
			<div class="content">
			<?php 
				$this->showBody();
			?>
			</div>
			<div class="footer"></div>
		</div>
	</body>
</html>