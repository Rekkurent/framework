<form id="input_form">
	<div class="header_add_note"><h2>Добавить заметку</h2></div>
	<div class="content_add_note">
		<label for="title">Заголовок:</label><br><input type="text" id="title" name="title"><br>
		<label for="note">Заметка:</label><br><textarea id="note" name="note"></textarea><br>
		<button type="submit" name="ajax_add" id="add_button">Добавить</button>
		<button type="submit" name="ajax_del_all" id="del_all_button">Удалить все заметки</button>
	</div>
</form>
<div align="center"><button id="showAll">Показать все Заметки</button></div>
<div class="notes">
	<div align="center" id="loader"><img  src="img/preloader.gif"></div>
</div>