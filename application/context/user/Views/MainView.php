<?php

namespace context\user\Views {
	
	use core\ViewHTML;
	
	class MainView extends ViewHTML {
		
		public function __construct() {
			
			parent::__construct();
			
			$this->title = "Main";
		
			$template = 'context/user/Templates/mainTemplate.php';
			$this->setTemplate('main', $template);

			$styles = [
				'/css/style.css'
			];

			$this->addStyles($styles);

		}
		
	}
	
}

?>