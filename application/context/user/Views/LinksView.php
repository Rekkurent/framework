<?php

namespace context\user\Views {
	
	class LinksView extends MainView {
		
		public function __construct() {
			
			parent::__construct();
			
			$this->title = "Ссылки";
			
			$scripts = [
				'/js/auth.js'
			];

			$this->addScripts($scripts);

		}
		
	}
	
}

?>