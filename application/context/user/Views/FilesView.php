<?php

namespace context\user\Views {
	
	class FilesView extends MainView {
		
		public function __construct() {
			
			parent::__construct();
			
			$this->title = "Файлы";
			
			$template = 'context/user/Templates/filesTemplate.php';
			$this->setTemplate('files', $template);
			
			$scripts = [
				'/js/auth.js',
				'/js/files.js'
			];
			


			$styles = [
				'/css/style.css',
				'/css/files.css'
			];
			
			$this->addScripts($scripts);
			$this->addStyles($styles);

		}
	
		public function showBody(){
			
			$this->showTemplate('files');
			
		}
		
		public function showDir(){
			
			$files = $this->data['files'];
			
			for($i=0; $i<count($files); $i++) {
				
				$file = $files[$i];
				$str =  
					'<li class="file"><a href="files.php?action=getFile&name='.urlencode($file).'">'.$file.'</a>
						<span class="menu">
							<img src="icons/delete_note_0.png" class="delete_file">
						</span>
					</li>';
					
				echo $str;
				
			}
			
		}
		
	}
	
}

?>