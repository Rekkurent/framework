<?php

namespace context\user\Views {
	
	class NotesView extends MainView {
		
		public function __construct() {
			
			parent::__construct();
			
			$this->title = "Заметки";
			
			$template = 'context/user/Templates/notesTemplate.php';
			$this->setTemplate('notes', $template);
			
			$scripts = [
				'/js/auth.js',
				'/js/notes.js'
			];

			$styles = [
				'/css/style.css',
				'/css/notes.css'
			];
			
			$this->addScripts($scripts);
			$this->addStyles($styles);

		}
	
		public function showBody(){
			
			if (
				isset($this->data['isAuth']) &&
				$this->data['isAuth'] == true
			) {
				
				$this->showTemplate('notes');
				
			}

		}
		
	}
	
}

?>