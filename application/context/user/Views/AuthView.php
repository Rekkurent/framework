<?php

namespace context\user\Views {
	
	class AuthView extends MainView {
		
		public function __construct() {
			
			parent::__construct();
			
			$this->title = "Авторизация";
			
			$template = 'context/user/Templates/authTemplate.php';
			$this->setTemplate('auth', $template);
			
			$scripts = [
				'/js/autha.js'
			];
			
			$styles = [
				'/css/auth.css'
			];
			
			$this->addScripts($scripts);
			$this->addStyles($styles);

		}
		
		public function showBody() {
			
			$this->showTemplate('auth');
			
		}
		
	}
	
}

?>