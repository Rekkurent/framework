<?php

namespace context\user\Views {
	
	class IndexView extends MainView {
		
		public function __construct() {
			
			parent::__construct();
			
			$this->title = "Главная";
			
			$scripts = [
				'/js/index.js',
				'/js/auth.js'
			];

			$styles = [
				'/css/index.css'
			];
			
			$this->addScripts($scripts);
			$this->addStyles($styles);

		}
		
	}
	
}

?>