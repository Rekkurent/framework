<?php

	ini_set('error_reporting', E_ALL);
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);

	$paths = [
		get_include_path(),
		dirname (__FILE__ )
	];
	
	$path = implode(PATH_SEPARATOR, $paths);

	set_include_path($path);

	//print_r("\n".$path."\n");

	function autoloader($class) {

		$file = str_replace('\\', DIRECTORY_SEPARATOR, $class).".php";
		//print_r("\n".$class."\n");
		//print_r("\n".$file."\n");
		require_once($file);
		
	}

	spl_autoload_register('autoloader');
	
?>